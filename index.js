/*
	Mini Activity:
		Create an expressjs API designated to port 4000
		Create a new route with endpoint /hello and method GET
			-Should be able to respond with "Hello World"
*/

const express = require("express");
const mongoose = require("mongoose")
//Mongoose is a package that allows creation of Schemas to model our data structure 
//Also has access to a number of methods for manipulation our database

const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://admin:admin123@cluster0.pjjaa.mongodb.net/B157_to-do?retryWrites=true&w=majority",
{
	useNewUrlParser:true,
	useUnifiedTopology: true
});

//Set notification for connection successed or failed
let db = mongoose.connection;
//if a connection error occured, this will be the output in the console
db.on("error", console.error.bind(console, "Connection error"));
//if the connection is successful, output in the console
db.once("open", () => console.log("We're connected to the cloud database"));

//Schemas determine the structure of the documents to be written in the database
//this acts as a blueprint to our data
const taskSchema = new mongoose.Schema(
	{
		name: String,
		status: {
			type: String,
			default: "pending"
		}
	}
);

//Model uses Schemas and are used to create/instantiate objects that correspond the the Schema
//A model must be in a singular form and capitalized
//1st parameter of the mongoose model method indicates the collection in where will be stored in the MongoDB collection
//2nd Parameter is used to specify the Schema/blueprint of the documents 
//Using mongoose, the package was programmed well enough that it automatically converts the singular form of the model name into plural form when creating a collection
const Task = mongoose.model("Task", taskSchema);

//Setup for allowing the server to handle data from requests
//Allows our app to read json data
app.use(express.json())
//Allows our app to read data from forms
app.use(express.urlencoded({extended:true}))

//Creating a new Task
/*
	1. Add a functionality to check if there are duplicate tasks
		- If the task is already exists in the database, we return an error
		- If the task tasks doesn't exist, we add it in our database.
	2. The task data will be coming from the request's body
	3. Create a Task object with a "name" field/property
*/
app.post("/tasks", (req,res) => {
	Task.findOne({name: req.body.name}, (err,result) => {
		if (result != null && result.name == req.body.name) {
			return res.send("Duplicate task found");
		} else {
			let newTask = new Task ({
				name: req.body.name 
			});
			newTask.save((saveErr, savedTask) => {
				if (saveErr) {
					return console.error(saveErr)
				} else {
					return res.status(201).send("New Task Created.")
				}
			})
		}
	})
});

//GET request to retrieve all the documents
/*
	1. Retrieve all the documents
	2. If an error is encountered, print the error
	3. If no errors are found, send a success back to client
	4. 
*/

app.get("/tasks", (req,res) => {
	Task.find({}, (err,result) => {
		if (err) {
			console.log(err)
		} else {
			return res.status(200).json({
				data: result
			})
		}
	})
})



//ACTIVITY
//1. Create a User Schema
const userSchema = new mongoose.Schema(
	{
		username: String,
		password: String
	}
);

//2. User Model
const User = mongoose.model("User", userSchema);

//3. 
app.post("/signup", (req,res) => {
	User.findOne({username: req.body.username}, (err,result) => {
		if (result != null && result.username == req.body.username) {
			return res.send("Username already exists. Please try with another one.");
		} else {
			let newUser = new User ({
				username: req.body.username,
				password: req.body.password
			});
			newUser.save((signUpErr, savedUser) => {
				if (signUpErr) {
					return console.error(signUpErr)
				} else {
					return res.status(201).send("New user registered.")
				}
			})
		}
	})
});


app.listen(port,() => console.log(`Server is running at port ${port}`) );